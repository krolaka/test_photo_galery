module ApplicationHelper
  def bootstrap_class_for(flash_type)
    hash = {
      success: 'alert-success',
      error: 'alert-danger',
      alert: 'alert-warning',
      notice: 'alert-info'
    }
    hash.stringify_keys[flash_type.to_s] || flash_type.to_s
  end

  def flash_messages(*)
    flash.each do |msg_type, messages|
      concat(
        content_tag(:div, messages, class: "alert #{bootstrap_class_for(msg_type)}", role: 'alert') do
          concat content_tag(:button, 'x', class: 'close', data: { dismiss: 'alert' })
          concat content_tag(
            :ul,
            messages.map do |m|
              content_tag(:li, m)
            end.join.html_safe,
            class: 'list-unstyled'
          )
        end
      )
    end
    nil
  end
end
