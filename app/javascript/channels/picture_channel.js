import consumer from "./consumer"

consumer.subscriptions.create("PictureChannel", {
  connected() {},
  disconnected() {},
  received(data) {
    var listPictures = $('.list-pictures')
    if (listPictures.length) {
      listPictures.prepend(data)
    }
  }
});
