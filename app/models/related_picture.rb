class RelatedPicture < Picture
  include PictureConcern

  validate :validate_mime_type
  validates :image, ratio: :related

  private

  def validate_mime_type
    primary_image = Picture.find(related_id).image
    return if image.content_type == primary_image.content_type

    errors.add(:image, "Must be #{primary_image.content_type} content type")
  end
end
