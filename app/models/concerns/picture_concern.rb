module PictureConcern
  extend ActiveSupport::Concern

  included do
    self.table_name = 'pictures'
    after_create_commit :set_average_color
    after_create_commit { PictureRelayJob.perform_later(self) }

    has_one_attached :image
    belongs_to :category

    scope :newest, -> { order(created_at: :desc) }
    scope :in_category, ->(id) { where(category_id: id) }
    scope :with_category, -> { joins(:category) }
    scope :with_related, ->(id) { where(related_id: id) }

    validates :name, presence: true, uniqueness: true
    validates :image, attached: true, content_type: %i[png jpg jpeg]
  end

  def set_average_color
    image_content = image.download
    self.average_color = Averagecolor.new(image_content).perform
    save
  end
end
