class PicturesController < ApplicationController
  def upload
    picture, path = switch_model
    unless picture.valid? && picture.save
      flash[:error] = picture.errors.full_messages
    end
    redirect_to path
  end

  def show
    @picture = Picture.find(params[:id])
    @pictures = RelatedPicture.with_related(params[:id]).with_category.newest
  end

  private

  def switch_model
    if upload_params[:category_id].blank?
      assign_picture
    else
      assign_related_picture
    end
  end

  def assign_picture
    picture = Picture.new(upload_params)
    picture.category_id = params[:id]
    path = category_path(params[:id])
    [picture, path]
  end

  def assign_related_picture
    picture = RelatedPicture.new(upload_params)
    picture.related_id = params[:id]
    path = picture_path(params[:id])
    [picture, path]
  end

  def upload_params
    params.require(:picture).permit(:image, :name, :category_id)
  end
end
