class HomeController < ApplicationController
  def index
    @pictures = Picture.with_category.newest
  end
end
