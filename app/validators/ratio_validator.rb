class RatioValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, _value)
    return true unless record.send(attribute).attached?

    changes = record.attachment_changes[attribute.to_s]
    return true if changes.blank?

    file = changes.attachable
    related_metadata = ActiveStorageValidations::Metadata.new(file).metadata
    primary_metadata = primary_metadata(record)
    valid_ratio?(record, attribute, primary_metadata, related_metadata)
  end

  def valid_ratio?(record, attribute, primary_metadata, related_metadata)
    return true unless neq_ratio?(primary_metadata, related_metadata)

    record.errors.add(attribute, add_message(ratio(primary_metadata)))
  end

  def primary_metadata(record)
    primary_image = Picture.find(record.related_id).image
    ActiveStorage::Analyzer::ImageAnalyzer.new(primary_image).metadata
  end

  private

  def add_message(ratio)
    "Must be #{ratio.join(':')} ratio"
  end

  def neq_ratio?(primary_metadata, related_metadata)
    ratio(primary_metadata) != ratio(related_metadata)
  end

  def ratio(metadata)
    width = metadata[:width]
    height = metadata[:height]
    mdc = mdc(width, height)
    [width / mdc, height / mdc]
  end

  def mdc(width, height)
    res = 0
    loop do
      res = width % height
      width = height
      height = res
      break if res.zero?
    end
    width
  end
end
