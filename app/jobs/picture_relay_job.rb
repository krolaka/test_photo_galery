class PictureRelayJob < ApplicationJob
  queue_as :default

  def perform(record)
    result = PicturesController.render(
      partial: 'shared/picture',
      assigns: { picture: record }
    )
    ActionCable.server.broadcast "picture_channel", result
  end
end