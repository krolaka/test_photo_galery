class Averagecolor
  def initialize(attachment)
    @tempfile = Tempfile.new('avc_tmp').binmode
    @tempfile << attachment
  end

  def perform
    image = MiniMagick::Image.new(@tempfile.path)
    image.colorspace 'LCHab'
    image.resize '1x1'
    pixel_array = image.get_pixels
    'rgb(' << pixel_array.join(',') << ')'
  rescue MiniMagick::Error => e
    puts e.inspect
    ''
  ensure
    @tempfile.flush
  end
end
