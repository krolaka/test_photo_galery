Rails.application.routes.draw do
  root to: 'home#index'

  resources :categories, except: %i[destroy update edit]
  resources :pictures, except: %i[index destroy create update edit] do
    post :upload, on: :member
  end
end
