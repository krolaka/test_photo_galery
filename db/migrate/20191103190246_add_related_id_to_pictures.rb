class AddRelatedIdToPictures < ActiveRecord::Migration[6.0]
  def change
    add_column :pictures, :related_id, :integer, null: true
  end
end
