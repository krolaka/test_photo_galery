class CreatePictures < ActiveRecord::Migration[6.0]
  def change
    create_table :pictures do |t|
      t.string :name, null: false
      t.string :average_color, null: false, default: ''
      t.timestamps
    end
    add_reference :pictures, :category, foreign_key: true
  end
end
